<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Embedding PHP in HTML</title>
        <link rel="stylesheet" href="" type="text/css" >
    </head>
    <body>
        
        <?php

            $name = "Ben";
            $age = 873;
            $isFeelingOld = false;

            if ($age > 900 )
            {
                $isFeelingOld = true;
            }

            $answer = $isFeelingOld ? 'true' : 'false';

            echo "It is $answer that $name is feeling old at $age";

        ?>

    </body>
</html>