<?php

$string = "The quick brown fox jumps over the lazy dog";

echo strlen($string)."<br><br>";
echo str_word_count($string)."<br><br>";
echo strrev($string)."<br><br>";
echo strpos($string, "z")."<br><br>";
echo substr_count($string, "jump")."<br><br>";
echo substr($string, 5, 19)."<br><br>";
echo str_replace($string, "jump", "walks")."<br><br>";

?>