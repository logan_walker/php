<?php


    $names = array( 
    ["name" => "Bob",
    "age" => 25, 
    "diploma" => "DAC", 
    "level" => 6],

    ["name" => "Joe",
    "age" => 34, 
    "diploma" => "DAC", 
    "level" => 6]
    );

    echo $names[1]["name"]."<br>";
    echo $names[0]["age"];

    echo "<br><br>";

    //This is what would happen in a loop
    foreach($names as $value)
    {
        echo $value["name"]."<br>";
    }

    echo "<br><br>";

    //Another loop to use is the while loop.

    $i = 0;

    while(count($names) > 0)
    {
        echo $names[$i]["name"]."<br>";
        $i++;
    }

?>