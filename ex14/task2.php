<?php

    $names = array(
        "Bob" => array ( 
        "age" => 25, 
        "diploma" => "DAC", 
        "level" => 6),

        "Joe" => array (  
        "age" => 34, 
        "diploma" => "DAC", 
        "level" => 6)
    );

    echo $names["Bob"]["age"]."<br>";
    echo $names["Joe"]["age"];

    echo "<br><br>";

    //This is what would happen in a loop
    foreach($names as $value)
    {
        echo $value["age"]."<br>";
    }

    echo "<br><br>";
?>