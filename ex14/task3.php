<?php

    $names = ["Bob", 2, "Joe", "Lucy"];

    echo "Display: print_r()"."<br>";
    print_r($names);

    echo "<br><br>";

    echo "Display: var_dump()"."<br>";
    var_dump($names);

?>