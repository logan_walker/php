<?php

    function getProduct($number1, $number2) {
        //inside a function we can put anything we want
        return $number1 * $number2;
    }

    //store the function in a variable first
    $results = getProduct(32, 64);
    echo $results;

    echo "<br><br>";

    //print the function directly
    echo getProduct(34, 82);
?>